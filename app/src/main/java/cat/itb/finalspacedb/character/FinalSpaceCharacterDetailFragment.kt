package cat.itb.finalspacedb.character

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import cat.itb.finalspacedb.R
import cat.itb.finalspacedb.viewmodel.FinalSpaceCharacterViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [FinalSpaceCharacterDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FinalSpaceCharacterDetailFragment : Fragment(R.layout.fragment_final_space_character_detail) {
    // TODO: Rename and change types of parameters
    private lateinit var nameTextView: TextView
    private lateinit var characterImageView: ImageView
    private lateinit var statusTextView: TextView
    private lateinit var specieTextView: TextView
    private lateinit var genderTextView: TextView
    private lateinit var hairTextView: TextView
    private lateinit var originTextView: TextView
    private lateinit var aliasTextView: TextView
    private lateinit var abilitiesTextView: TextView

    private var characterPosition: Int?=null
    private var finalSpaceCharacter: FinalSpaceCharacter?  = null
private val characterViewModel: FinalSpaceCharacterViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments?.isEmpty != true){
            characterPosition = arguments!!.get("characterPosition") as Int
            finalSpaceCharacter = characterViewModel.getCharacters()[characterPosition!!]
        }

        nameTextView = view.findViewById(R.id.characterNameDetail_textview)
        characterImageView = view.findViewById(R.id.character_imageView)
        statusTextView = view.findViewById(R.id.status_textview)
        specieTextView = view.findViewById(R.id.specie_textView)
        genderTextView = view.findViewById(R.id.gender_textview)
        hairTextView = view.findViewById(R.id.hair_textView)
        originTextView = view.findViewById(R.id.characterOriginDetail_textview)
        aliasTextView = view.findViewById(R.id.characterAliasDetail_textview)
        abilitiesTextView = view.findViewById(R.id.characterAbilitiesDetail_textview)

        if (finalSpaceCharacter != null){
            nameTextView.setText(finalSpaceCharacter!!.name)
            characterImageView.setImageResource(finalSpaceCharacter!!.snapshot)
            statusTextView.setText(finalSpaceCharacter!!.status)
            specieTextView.setText(finalSpaceCharacter!!.species)
            genderTextView.setText(finalSpaceCharacter!!.gender)
            hairTextView.setText(finalSpaceCharacter!!.hair)
            originTextView.setText(finalSpaceCharacter!!.origin)
            aliasTextView.setText(finalSpaceCharacter!!.alias)
            abilitiesTextView.setText(finalSpaceCharacter!!.abilities)
        }

    }


}