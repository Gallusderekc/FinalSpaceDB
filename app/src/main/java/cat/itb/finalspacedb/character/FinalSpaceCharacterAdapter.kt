package cat.itb.finalspacedb.character

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import cat.itb.finalspacedb.R

class FinalSpaceCharacterAdapter(characters: List<FinalSpaceCharacter>) :
    RecyclerView.Adapter<FinalSpaceCharacterAdapter.FinalSpaceCharacterViewHolder>() {
    private val characters = characters


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FinalSpaceCharacterViewHolder {
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fragment_final_space_item, parent, false)
        return FinalSpaceCharacterViewHolder(v)
    }

    override fun onBindViewHolder(holder: FinalSpaceCharacterViewHolder, position: Int) {
        holder.bindData(characters[position])
        holder.itemView.setOnClickListener {
            val directions =
                FinalSpaceCharacterListFragmentDirections.actionItemFragmentToFinalSpaceCharacterDetailFragment(
                    position
                )
            Navigation.findNavController(it).navigate(directions)
        }
    }

    override fun getItemCount(): Int {
        // Retorno la mida completa de la llista
        return characters.size
    }


    class FinalSpaceCharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var snapImageButton: ImageView
        private var nameTextView: TextView
        private var originTextView: TextView
        private var genderTextView: TextView
        private var statusTextView: TextView

        init {
            snapImageButton = itemView.findViewById(R.id.snap_imagebutton)
            nameTextView = itemView.findViewById(R.id.characterNameDetail_textview)
            originTextView = itemView.findViewById(R.id.characterOriginDetail_textview)
            genderTextView = itemView.findViewById(R.id.gender_textview)
            statusTextView = itemView.findViewById(R.id.status_textview)
        }

        fun bindData(finalSpaceCharacter: FinalSpaceCharacter) {
            snapImageButton.setImageResource(finalSpaceCharacter.snapshot)
            nameTextView.text = finalSpaceCharacter.name
            genderTextView.text = finalSpaceCharacter.gender
            originTextView.text = finalSpaceCharacter.origin
            statusTextView.text = finalSpaceCharacter.status
        }
    }
}
