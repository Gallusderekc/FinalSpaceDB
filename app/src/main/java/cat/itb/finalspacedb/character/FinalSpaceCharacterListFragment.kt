package cat.itb.finalspacedb.character

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cat.itb.finalspacedb.R
import cat.itb.finalspacedb.viewmodel.FinalSpaceCharacterViewModel

class FinalSpaceCharacterListFragment: Fragment(R.layout.fragment_character_recycler_view) {

    private lateinit var  recyclerView: RecyclerView

    private val characterViewModel: FinalSpaceCharacterViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = FinalSpaceCharacterAdapter(characterViewModel.getCharacters())



    }


}