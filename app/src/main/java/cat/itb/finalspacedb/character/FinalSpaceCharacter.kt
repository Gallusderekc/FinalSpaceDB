package cat.itb.finalspacedb.character

class FinalSpaceCharacter(val id:Int, val name:String, val status:String, val species:String, val gender:String, val hair:String, val alias: String, val origin:String, val abilities:String, val snapshot:Int) {

}
