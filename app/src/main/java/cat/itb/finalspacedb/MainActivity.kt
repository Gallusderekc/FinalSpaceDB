package cat.itb.finalspacedb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import cat.itb.finalspacedb.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.splashScreenTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val bottomNavigationView = binding.bottomNavigation
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        val navController = navHostFragment.navController
        bottomNavigationView.setupWithNavController(navController)

        val appBarConfiguration = AppBarConfiguration(setOf( R.id.searchFragment, R.id.favouritesFragment))
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.buttonCharacters.setOnClickListener{
            navController.navigate(R.id.action_menuFragment_to_itemFragment)
            binding.buttonCharacters.isEnabled=false
            binding.buttonLocations.isEnabled=true
        }
        binding.buttonLocations.setOnClickListener{
            navController.navigate(R.id.action_menuFragment_to_finalSpaceLocationListFragment)
            binding.buttonCharacters.isEnabled=true
            binding.buttonLocations.isEnabled=false

        }
        /*binding.buttonLocations.setOnClickListener{
            navController.navigate(R.id.action_itemFragment_to_finalSpaceLocationListFragment)
            binding.buttonCharacters.isEnabled=true
            binding.buttonLocations.isEnabled=false
        }
        binding.buttonLocations.setOnClickListener{
            navController.navigate(R.id.action_finalSpaceLocationListFragment_to_characterItemFragment)
            binding.buttonCharacters.isEnabled=false
            binding.buttonLocations.isEnabled=true
        }*/

    }

    /*override fun onResume() {
        super.onResume()
    }*/

    override fun onBackPressed() {
        super.onBackPressed()
        binding.buttonCharacters.isEnabled=true
        binding.buttonLocations.isEnabled=true
    }
}