package cat.itb.finalspacedb.location

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cat.itb.finalspacedb.R
import cat.itb.finalspacedb.viewmodel.FinalSpaceLocationViewModel


/**
 * A simple [Fragment] subclass.
 * Use the [FinalSpaceLocationListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FinalSpaceLocationListFragment : Fragment(R.layout.fragment_location_recycler_view) {
    // TODO: Rename and change types of parameters
    private lateinit var recyclerView: RecyclerView
    private val locationViewModel: FinalSpaceLocationViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        recyclerView=view.findViewById(R.id.locationRecyclerView)
        recyclerView.layoutManager=LinearLayoutManager(context)
        recyclerView.adapter = FinalSpaceLocationAdapter(locationViewModel.getLocations())
    }

}