package cat.itb.finalspacedb.location

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import cat.itb.finalspacedb.R

class FinalSpaceLocationAdapter(locations: List<FinalSpaceLocation>) :
    RecyclerView.Adapter<FinalSpaceLocationAdapter.FinalSpaceLocationViewHolder>() {
    private val locations = locations

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FinalSpaceLocationViewHolder {
        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fragment_final_space_location_item, parent, false)
        return FinalSpaceLocationViewHolder(v)
    }

    override fun onBindViewHolder(holder: FinalSpaceLocationViewHolder, position: Int) {
        holder.bindData(locations[position])
        //implementar itemView
        holder.itemView.setOnClickListener {
            val directions =
                FinalSpaceLocationListFragmentDirections.actionFinalSpaceLocationListFragmentToFinalSpaceLocationDetailFragment(
                    position
                )
            Navigation.findNavController(it).navigate(directions)

        }
    }

    override fun getItemCount(): Int {
        // Retorno la mida completa de la llista
        return locations.size
    }


    class FinalSpaceLocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var snapImageLocation: ImageView
        private var nameTextView: TextView
        private var typeTextView: TextView


        init {
            snapImageLocation = itemView.findViewById(R.id.snap_imageLocation)
            nameTextView = itemView.findViewById(R.id.locationName_textview)
            typeTextView = itemView.findViewById(R.id.locationType_textview)
        }

        fun bindData(finalSpaceLocation: FinalSpaceLocation) {
            snapImageLocation.setImageResource(finalSpaceLocation.snapshot)
            nameTextView.text = finalSpaceLocation.name
            typeTextView.text = finalSpaceLocation.type
        }
    }
}