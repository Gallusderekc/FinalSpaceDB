package cat.itb.finalspacedb.location

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import cat.itb.finalspacedb.R
import cat.itb.finalspacedb.viewmodel.FinalSpaceCharacterViewModel
import cat.itb.finalspacedb.viewmodel.FinalSpaceLocationViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [FinalSpaceLocationDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FinalSpaceLocationDetailFragment : Fragment(R.layout.fragment_final_space_location_detail) {
    // TODO: Rename and change types of parameters
    private lateinit var locationNameTextView: TextView
    private lateinit var locationImageView: ImageView
    private lateinit var locationTypeTextView: TextView
    private lateinit var locationInhabitantsTextView: TextView
    private lateinit var locationNotableResidentsTextView: TextView

    private var locationPosition: Int? = null
    private var finalSpaceLocation: FinalSpaceLocation? = null
    private val locationViewModel: FinalSpaceLocationViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments?.isEmpty != true) {
            locationPosition = arguments!!.get("locationPosition") as Int
            finalSpaceLocation = locationViewModel.getLocations()[locationPosition!!]
        }
        locationNameTextView = view.findViewById(R.id.locationNameDetail_textview)
        locationImageView = view.findViewById(R.id.location_imageView)
        locationTypeTextView = view.findViewById(R.id.locationTypeDetail_textview)
        locationInhabitantsTextView = view.findViewById(R.id.locationInhabitantsDetail_textview)
        locationNotableResidentsTextView =
            view.findViewById(R.id.locationNotableResidentsDetail_textview)

        if (finalSpaceLocation != null) {
            locationNameTextView.setText(finalSpaceLocation!!.name)
            locationImageView.setImageResource(finalSpaceLocation!!.snapshot)
            locationTypeTextView.setText(finalSpaceLocation!!.type)
            locationInhabitantsTextView.setText(finalSpaceLocation!!.inhabitants)
            locationNotableResidentsTextView.setText(finalSpaceLocation!!.notableResidents)
        }
    }
}