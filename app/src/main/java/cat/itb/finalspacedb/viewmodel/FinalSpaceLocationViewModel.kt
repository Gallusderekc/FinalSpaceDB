package cat.itb.finalspacedb.viewmodel

import androidx.lifecycle.ViewModel
import cat.itb.finalspacedb.R
import cat.itb.finalspacedb.character.FinalSpaceCharacter
import cat.itb.finalspacedb.location.FinalSpaceLocation
import kotlin.random.Random

class FinalSpaceLocationViewModel:ViewModel() {
    private var locations = mutableListOf<FinalSpaceLocation>()

    private var snapshots= arrayOf(
        R.drawable.earth,
        R.drawable.tera_con_prime
    )

    init{
        for(i in 1..100){
            locations.add(
                FinalSpaceLocation(i,snapshots[Random.nextInt(2)],"LOCATION #$i",
                    "TYPE", "INHABITANTS", "NOTABLE RESIDENTS"
                    )
            )
        }
    }

    fun getLocations()=locations
}