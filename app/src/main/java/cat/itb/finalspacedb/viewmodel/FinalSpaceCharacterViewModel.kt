package cat.itb.finalspacedb.viewmodel

import androidx.lifecycle.ViewModel
import cat.itb.finalspacedb.character.FinalSpaceCharacter
import cat.itb.finalspacedb.R
import kotlin.random.Random

class FinalSpaceCharacterViewModel:ViewModel() {
    private var characters = mutableListOf<FinalSpaceCharacter>()

    private var snapshots= arrayOf(
        R.drawable.personatge1,
        R.drawable.personatge2
    )

    init{
        for(i in 1..100){
            characters.add(
                FinalSpaceCharacter(i,"CHARACTER #$i",
                "STATUS", "SPECIE", "GENDER",
                "HAIR", "ALIAS", "ORIGIN",
                "ABILITIES", snapshots[Random.nextInt(2)])
            )
        }
    }

    fun getCharacters()=characters
}
